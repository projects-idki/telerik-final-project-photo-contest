package com.telerikfinalproject.photocontest.services;

import com.telerikfinalproject.photocontest.models.Contest;
import com.telerikfinalproject.photocontest.models.Photo;
import com.telerikfinalproject.photocontest.models.Review;
import com.telerikfinalproject.photocontest.models.User;
import com.telerikfinalproject.photocontest.services.contracts.ContestService;
import com.telerikfinalproject.photocontest.services.contracts.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.telerikfinalproject.photocontest.services.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ScheduledServiceTests {

    private Contest contest;

    @Mock
    ContestService contestService;
    @Mock
    UserService userService;

    @InjectMocks
    ScheduledService scheduledService;

    @BeforeEach
    private void makeContest() {
        contest = createContest();
        User user = createUser(1);
        Photo photo = createPhoto(1);
        Review review = createReview();
        photo.getReviewSet().add(review);
        photo.setUser(user);
        contest.getPhotoSet().add(photo);
    }


    @Test
    public void updateRankingPoints_Should_Call_Repository_When_Valid() {
        List<Contest> contestList = new ArrayList<>(Arrays.asList(createContest(), createContest()));
        when(contestService.getFinishingContests()).thenReturn(contestList);

        scheduledService.updateRankingPoints();
        verify(contestService, times(2)).updateContest(createContest());

    }

    @Test
    public void updateRankingPoints_Should_Change_Finished_Field_ToTrue() {
        Contest contest = createContest();
        contest.setFinished(false);
        List<Contest> contestList = new ArrayList<>(Arrays.asList(createContest(), contest));
        when(contestService.getFinishingContests()).thenReturn(contestList);

        scheduledService.updateRankingPoints();

        assertTrue(contest.isFinished());
    }
}
